Загружаем любой лайвсд с дебианом.

Открываем терминал, логинимся под рута, размечаем диск через fdisk(при желании можно установить gparted).
Бут, корень, хоум, свап. Под бут 200 мегабайт, под корень - 15G(15360мб), свап, хоум - сколько душе угодно.
 
mkfs.ext2 /dev/sda1; mkfs.ext4 /dev/sda2; mkfs.ext4 /dev/sda4
mkswap /dev/sda3; swapon /dev/sda3
mount /dev/sda2 /mnt; mkdir /mnt/home; mkdir /mnt/boot
mount /dev/sda1 /mnt/boot; mount /dev/sda4 /mnt/home
 
Далее устанавливаем debootstrap и запускаем установку.
 
apt-get install debootstrap
debootstrap --arch $ARCH $RELEASE /mnt http://ftp.ru.debian.org/debian
 
, где $ARCH - архитектура(amd64/i386), $RELEASE - релиз(sid в данный момент)
 
Ждем пару минут, установка завершена.
Чрутаемся в голую систему, перед этим примонтировав нужные нам фс:
 
for i in /dev /dev/pts /proc /sys /run; do mount -B $i /mnt$i; done
chroot /mnt
 
Настраиваем apt и обновляем список:
 
echo "deb http://ftp.ru.debian.org/debian/ sid main contrib non-free" > /etc/apt/sources.list
 
touch /etc/apt/apt.conf
echo -e 'APT::Install-Recommends "0";\nAPT::Install-Suggests "0";\nAPT::Default-Release "sid";' > /etc/apt/apt.conf
 
apt update
 
Настраиваем сеть через провод:
touch /etc/systemd/network/net.network
echo -e "[Match]\nName=eth*\n\n[Network]\nDHCP=yes" > /etc/systemd/network/net.network
 
touch /etc/udev/rules.d/70-persistent-net.rules
echo 'SUBSYSTEM=="net", ACTION=="add", DRIVERS=="?*", ATTR{address}=="YOUR_MAC", ATTR{dev_id}=="0x0", ATTR{type}=="1", KERNEL=="eth*", NAME="eth0"' > /etc/udev/rules.d/70-persistent-net.rules
 
где YOUR_MAC - мак устройства, смотрим через ip a
 
Добавляем в автозагрузку:
systemctl enable systemd-networkd
 
Устанавливаем ядро, initramfs, и остальное нужное
apt install bash-completion linux-image-amd64 initramfs-tools grub2
 
Во время установки граба он предложит выбрать место для загрузчика, выбираем /dev/sda.
Посленастройка
cp /etc/skel/.bashrc ~
update-initramfs -u
grub-mkconfig -o /boot/grub/grub.cfg
 
Если нужны блобы для радевона. например
apt install firmware-amd-graphics
 
Задаём пароль руту: passwd
 
Правим /etc/fstab
echo -e "/dev/sda2 / ext4 defaults 0 0\n/dev/sda4 /home ext4 defaults 0 1\n/dev/sda3 none swap defaults 0 0\n/dev/sda1 /boot ext2 defaults 0 0" >> /etc/fstab
 
Справка: даже если вы забудете сделать fstab, то система все равно загрузится, после логина mount -o remount,rw /, и уже правите fstab
 
Задаем хостнейм.
echo '' > /etc/hostname
 
Ребутаемся, и радуемся.
 
Дополнение:
 
Настройка локалей:
apt install locales
cat /dev/null > /etc/locale.gen
dpkg-reconfigure locales
 
Для синхронизации времени есть systemd-timesyncd.
 
Для монтирования можно использовать UUID, blkid в помощь.
 
Если же имеется SSD, то можно добавить в опции монтирования noatime, а так же включить еженедельный трим:
 
cp /usr/share/doc/util-linux/examples/fstrim.{service,timer} /etc/systemd/system
systemctl enable fstrim.timer
 
Так же можно тримить вручную, fstrim -a для всех точек монтирования или fstrim -v /mount/point